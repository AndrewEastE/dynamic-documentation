# Dynamic Documentation



## How to Edit:

### Edit Online

1. Click the edit button at the top of this file
2. Make edits to the text file
   - See the [GitLab docs on their flavour of Markdown](https://handbook.gitlab.com/docs/markdown-guide/) for how to add formatting, lists, links, embedded images, code blocks, etc.
3. Under "Commit message", type a brief sentence that describes what your changes accomplish (which should start with a verb), such as "Adds a 'how-to-edit' section"
4. Click "Commit change"!

### Edit on Your Computer

1. Clone the repo with git
2. Edit the text files with your favorite editor. Many editors support previewing Markdown formatting!
3. Commit and push the changes to the text files


## Testing features of GitLab documents

### Local Links

Can we use links to [other files in the repository](onboarding/onboarding-checklist.md)?
- [ ] Yes
- [ ] No

Can we use links to [other dir in the repository](onboarding/)?
- [ ] Yes
- [ ] No

### Code Blocks

```go
package main

import "fmt"

func main() {
    fmt.Println("hello world")
}
```

```bash
go run hello-world.go
# hello world	

go build hello-world.go
ls
# hello-world    hello-world.go

./hello-world
# hello world
```

### Images

Can we embed images?
![Picture of Godzilla I found on the internet](https://cdn.vox-cdn.com/thumbor/kUP4KZKm71D0A9SQMOv2UOrbpss=/0x0:3000x2000/2570x1028/filters:focal(1290x723:1770x1203):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/72871174/Evolution_of_Godzilla_2.0.jpg)*From an absolute URL on the internet*

![Picture of Godzilla inside this repo](team/godzilla-using-laptop.png)*From a PNG in this repo*

